package com.example.mydemo4_colorcode.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mydemo4_colorcode.R;

public class DashboardActivity extends AppCompatActivity {

    // Text view for goint into new activity
    LinearLayout activity_dashboard_tvCMYKtoRGB, activity_dashboard_tvHSVtoRGB, activity_dashboard_tvRGBtoCMYK, activity_dashboard_tvRGBtoHSV;

    // Intent object
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();
        process();
        listeners();
    }


    private void init() {

        //Text view bind with id
        activity_dashboard_tvCMYKtoRGB = findViewById(R.id.activity_dashboard_tvCMYKtoRGB);
        activity_dashboard_tvHSVtoRGB = findViewById(R.id.activity_dashboard_tvHSVtoRGB);
        activity_dashboard_tvRGBtoCMYK = findViewById(R.id.activity_dashboard_tvRGBtoCMYK);
        activity_dashboard_tvRGBtoHSV = findViewById(R.id.activity_dashboard_tvRGBtoHSV);
    }


    private void process() {
    }


    private void listeners() {

        // Add activity on click
        activity_dashboard_tvCMYKtoRGB.setOnClickListener(v -> {
            intent = new Intent(DashboardActivity.this, cmyk_rgbActivity.class);
            startActivity(intent);
        });
        activity_dashboard_tvHSVtoRGB.setOnClickListener(v -> {
            intent = new Intent(DashboardActivity.this, hsv_rgbActivity.class);
            startActivity(intent);
        });
        activity_dashboard_tvRGBtoCMYK.setOnClickListener(v -> {
            intent = new Intent(DashboardActivity.this, rgb_cmykActivity.class);
            startActivity(intent);
        });
        activity_dashboard_tvRGBtoHSV.setOnClickListener(v -> {
            intent = new Intent(DashboardActivity.this, rgb_hsvActivity.class);
            startActivity(intent);
        });
    }
}