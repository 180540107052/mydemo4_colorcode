package com.example.mydemo4_colorcode.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mydemo4_colorcode.R;

public class rgb_cmykActivity extends AppCompatActivity {

    EditText activity_rgb_cmyk_etR,activity_rgb_cmyk_etG,activity_rgb_cmyk_etB;
    Button activity_rgb_cmyk_btn;
    TextView activity_rgb_cmyk_tvcc,activity_rgb_cmyk_tvmm,activity_rgb_cmyk_tvyy,activity_rgb_cmyk_tvkk,
            activity_rgb_cmyk_tvc,activity_rgb_cmyk_tvm,activity_rgb_cmyk_tvy,activity_rgb_cmyk_tvk,
            activity_rgb_cmyk_tvmid1,activity_rgb_cmyk_tvmid2,activity_rgb_cmyk_tvmid3,activity_rgb_cmyk_tvmid4;
    LinearLayout activity_rgb_cmyk_layout;

    //variable for get data
    int r;
    int g;
    int b;

    float max;
    float cyan = 0.0f;
    float magenta = 0.0f;
    float yellow = 0.0f;
    float black = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rgb_cmyk);

        init();
        process();
        listeners();





    }

    public void convertRGBtoCMYK(int red, int green, int blue)
    {
        float r= red;
        float g= green;
        float b= blue;
        Log.d("test", "r = " + r);
        Log.d("test", "g = " + g);
        Log.d("test", "b = " + b);

        if(r == 0 && g == 0 && b == 0)
        {
            cyan   = magenta = yellow = 0;
            black  = 1;
            Log.d("if test", "cyan = " + cyan);
            Log.d("if test", "magenta = " + magenta);
            Log.d("if test", "yellow = " + yellow);
            Log.d("if test", "black = " + black);
        }

        else {
            r    = r/255;
            g  = g/255;
            b   = b/255;

            Log.d("else test", "r = " + r);
            Log.d("else test", "g = " + g);
            Log.d("else test", "b = " + b);

            max    = r;
            if(g > max)
            {
                max = g;
            }

            if(b > max)
            {
                max = b;
            }

            float white   = max;

            cyan    = (white - r)  / white;
            magenta = (white - g)/ white;
            yellow  = (white - b) / white;

            black   = 1 - white;
            Log.d("color", "C=" + cyan);
            Log.d("color", "M=" + magenta);
            Log.d("color", "Y=" + yellow);
            Log.d("color", "K=" + black);
        }
        cyan=Math.round(cyan*100);
        magenta=Math.round(magenta*100);
        yellow=Math.round(yellow*100);
        black=Math.round(black*100);
        int fr = 255-red;
        int fg = 255-green;
        int fb = 255-blue;
        displayCMYK(cyan,magenta,yellow,black,red,green,blue,fr,fg,fb);





    }

    public void displayCMYK(float cyan,float magenta,float yellow,float black,int red,int green,int blue,int fr,int fg,int fb){
        activity_rgb_cmyk_tvcc = findViewById(R.id.activity_rgb_cmyk_tvcc);
        activity_rgb_cmyk_tvmm = findViewById(R.id.activity_rgb_cmyk_tvmm);
        activity_rgb_cmyk_tvyy = findViewById(R.id.activity_rgb_cmyk_tvyy);
        activity_rgb_cmyk_tvkk = findViewById(R.id.activity_rgb_cmyk_tvkk);
        activity_rgb_cmyk_tvc = findViewById(R.id.activity_rgb_cmyk_tvc);
        activity_rgb_cmyk_tvm = findViewById(R.id.activity_rgb_cmyk_tvm);
        activity_rgb_cmyk_tvy = findViewById(R.id.activity_rgb_cmyk_tvy);
        activity_rgb_cmyk_tvk = findViewById(R.id.activity_rgb_cmyk_tvk);
        activity_rgb_cmyk_tvmid1=findViewById(R.id.activity_rgb_cmyk_tvmid1);
        activity_rgb_cmyk_tvmid2=findViewById(R.id.activity_rgb_cmyk_tvmid2);
        activity_rgb_cmyk_tvmid3=findViewById(R.id.activity_rgb_cmyk_tvmid3);
        activity_rgb_cmyk_tvmid4=findViewById(R.id.activity_rgb_cmyk_tvmid4);

        activity_rgb_cmyk_layout=findViewById(R.id.activity_rgb_cmyk_layout);
        TextView activity_rgb_cmyk_tvcc=(TextView) findViewById(R.id.activity_rgb_cmyk_tvcc);
        TextView activity_rgb_cmyk_tvmm=(TextView) findViewById(R.id.activity_rgb_cmyk_tvmm);
        TextView activity_rgb_cmyk_tvyy=(TextView) findViewById(R.id.activity_rgb_cmyk_tvyy);
        TextView activity_rgb_cmyk_tvkk=(TextView) findViewById(R.id.activity_rgb_cmyk_tvkk);
        TextView activity_rgb_cmyk_tvc=(TextView) findViewById(R.id.activity_rgb_cmyk_tvc);
        TextView activity_rgb_cmyk_tvm=(TextView) findViewById(R.id.activity_rgb_cmyk_tvm);
        TextView activity_rgb_cmyk_tvy=(TextView) findViewById(R.id.activity_rgb_cmyk_tvy);
        TextView activity_rgb_cmyk_tvk=(TextView) findViewById(R.id.activity_rgb_cmyk_tvk);
        TextView activity_rgb_cmyk_tvmid1=(TextView) findViewById(R.id.activity_rgb_cmyk_tvmid1);
        TextView activity_rgb_cmyk_tvmid2=(TextView) findViewById(R.id.activity_rgb_cmyk_tvmid2);
        TextView activity_rgb_cmyk_tvmid3=(TextView) findViewById(R.id.activity_rgb_cmyk_tvmid3);
        TextView activity_rgb_cmyk_tvmid4=(TextView) findViewById(R.id.activity_rgb_cmyk_tvmid4);

        activity_rgb_cmyk_layout.setBackgroundColor(Color.rgb(red, green, blue));
        activity_rgb_cmyk_tvcc.setText(""+cyan);
        activity_rgb_cmyk_tvmm.setText(""+magenta);
        activity_rgb_cmyk_tvyy.setText(""+yellow);
        activity_rgb_cmyk_tvkk.setText(""+black);
        activity_rgb_cmyk_tvcc.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvmm.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvyy.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvkk.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvc.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvm.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvy.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvk.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvmid1.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvmid2.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvmid3.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_cmyk_tvmid4.setTextColor(Color.rgb(fr,fg,fb));




    }
    public void init() {
        activity_rgb_cmyk_etR = findViewById(R.id.activity_rgb_cmyk_etR);
        activity_rgb_cmyk_etG = findViewById(R.id.activity_rgb_cmyk_etG);
        activity_rgb_cmyk_etB = findViewById(R.id.activity_rgb_cmyk_etB);
        // activity_rgb_cmyk_btn=findViewById(R.id.activity_rgb_cmyk_btn);

    }

    private void process() {


    }

    private void listeners() {
        /* activity_rgb_cmyk_etR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("length", "length = " + activity_rgb_cmyk_etR.length());
                getDataFromField();

            }
        });*/

        activity_rgb_cmyk_etR.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activity_rgb_cmyk_etG.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        activity_rgb_cmyk_etB.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void getDataFromField(){
        //just get data ans set into temp variable
        if(activity_rgb_cmyk_etR.getText().toString().isEmpty() && activity_rgb_cmyk_etG.getText().toString().isEmpty() && activity_rgb_cmyk_etB.getText().toString().isEmpty()){
            convertRGBtoCMYK(0,0,0);
        }
        else if(activity_rgb_cmyk_etG.getText().toString().isEmpty() && activity_rgb_cmyk_etB.getText().toString().isEmpty()){
            r = Integer.parseInt(activity_rgb_cmyk_etR.getText().toString());
            if(r>255){
                activity_rgb_cmyk_etR.setText(String.valueOf(255));
                r=255;
            }
            convertRGBtoCMYK(r,0,0);

        }
        else if(activity_rgb_cmyk_etR.getText().toString().isEmpty() && activity_rgb_cmyk_etB.getText().toString().isEmpty()){
            g = Integer.parseInt(activity_rgb_cmyk_etG.getText().toString());
            if(g>255){
                activity_rgb_cmyk_etG.setText(String.valueOf(255));
                g=255;
            }
            convertRGBtoCMYK(0,g,0);

        }
        else if(activity_rgb_cmyk_etR.getText().toString().isEmpty() && activity_rgb_cmyk_etG.getText().toString().isEmpty()){
            b = Integer.parseInt(activity_rgb_cmyk_etB.getText().toString());
            if(b>255){
                activity_rgb_cmyk_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoCMYK(0,0,b);

        }
        else if(activity_rgb_cmyk_etR.getText().toString().isEmpty()){
            g = Integer.parseInt(activity_rgb_cmyk_etG.getText().toString());
            if(g>255){
                activity_rgb_cmyk_etG.setText(String.valueOf(255));
                g=255;
            }
            b = Integer.parseInt(activity_rgb_cmyk_etB.getText().toString());
            if(b>255){
                activity_rgb_cmyk_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoCMYK(0,g,b);

        }
        else if(activity_rgb_cmyk_etG.getText().toString().isEmpty()){
            r = Integer.parseInt(activity_rgb_cmyk_etR.getText().toString());
            if(r>255){
                activity_rgb_cmyk_etR.setText(String.valueOf(255));
                r=255;
            }
            b = Integer.parseInt(activity_rgb_cmyk_etB.getText().toString());
            if(b>255){
                activity_rgb_cmyk_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoCMYK(r,0,b);

        }
        else if(activity_rgb_cmyk_etB.getText().toString().isEmpty()){
            r = Integer.parseInt(activity_rgb_cmyk_etR.getText().toString());
            if(r>255){
                activity_rgb_cmyk_etR.setText(String.valueOf(255));
                r=255;
            }
            g = Integer.parseInt(activity_rgb_cmyk_etG.getText().toString());
            if(g>255){
                activity_rgb_cmyk_etG.setText(String.valueOf(255));
                g=255;
            }
            convertRGBtoCMYK(r,g,0);

        }
        else{
            r = Integer.parseInt(activity_rgb_cmyk_etR.getText().toString());
            if(r>255){
                activity_rgb_cmyk_etR.setText(String.valueOf(255));
                r=255;
            }
            g = Integer.parseInt(activity_rgb_cmyk_etG.getText().toString());
            if(g>255){
                activity_rgb_cmyk_etG.setText(String.valueOf(255));
                g=255;
            }
            b = Integer.parseInt(activity_rgb_cmyk_etB.getText().toString());
            if(b>255){
                activity_rgb_cmyk_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoCMYK(r,g,b);
        }

        Log.d("rgb_hsv_test", "R=" + r);
        Log.d("rgb_hsv_test", "G=" + g);
        Log.d("rgb_hsv_test", "B=" + b);



    }
}