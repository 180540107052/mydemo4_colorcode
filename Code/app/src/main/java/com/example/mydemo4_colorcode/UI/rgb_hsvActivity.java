package com.example.mydemo4_colorcode.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mydemo4_colorcode.R;

import java.text.DecimalFormat;

public class rgb_hsvActivity extends AppCompatActivity {

    EditText activity_rgb_hsv_etR,activity_rgb_hsv_etG,activity_rgb_hsv_etB;
    Button activity_rgb_hsv_btn;
    TextView activity_rgb_hsv_tvHH,activity_rgb_hsv_tvSS,activity_rgb_hsv_tvVV,
            activity_rgb_hsv_tvH,activity_rgb_hsv_tvS,activity_rgb_hsv_tvV,
            activity_rgb_hsv_tvmid1,activity_rgb_hsv_tvmid2,activity_rgb_hsv_tvmid3;
    LinearLayout activity_rgb_hsv_layout;


    int r;
    int g;
    int b ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rgb_hsv);

        init();
        process();
        listeners();




    }

    public void convertRGBtoHSV(int red, int green, int blue){
        // R, G, B values are divided by 255
        // to change the range from 0..255 to 0..1
        float r=red;
        float g=green;
        float b=blue;
        Log.d("rgb_hsv_test", "R=" + r);
        Log.d("rgb_hsv_test", "G=" + g);
        Log.d("rgb_hsv_test", "B=" + b);
        r = r / 255;
        g = g / 255;
        b = b / 255;
        Log.d("rgb_hsv_test", "R=" + r);
        Log.d("rgb_hsv_test", "G=" + g);
        Log.d("rgb_hsv_test", "B=" + b);

        // h, s, v = hue, saturation, value
        float cmax = Math.max(r, Math.max(g, b)); // maximum of r, g, b
        float cmin = Math.min(r, Math.min(g, b)); // minimum of r, g, b
        float diff = cmax - cmin; // diff of cmax and cmin.
        float h = -1, s = -1;

        // if cmax and cmax are equal then h = 0
        if (cmax == cmin)
            h = 0;

            // if cmax equal r then compute h
        else if (cmax == r)
            h = (60 * ((g - b) / diff) + 360) % 360;

            // if cmax equal g then compute h
        else if (cmax == g)
            h = (60 * ((b - r) / diff) + 120) % 360;

            // if cmax equal b then compute h
        else if (cmax == b)
            h = (60 * ((r - g) / diff) + 240) % 360;

        // if cmax equal zero
        if (cmax == 0)
            s = 0;
        else
            s = (diff / cmax) * 100;

        // compute v
        float v = cmax * 100;
        Log.d("rgb_hsv_test", "H=" + h);
        Log.d("rgb_hsv_test", "S=" + s);
        Log.d("rgb_hsv_test", "V=" + v);
        h=(int)(Math.round(h));

        int fr = 255-red;
        int fg = 255-green;
        int fb = 255-blue;



        displayHSV(h,s,v,red,green,blue,fr,fg,fb);


}
    public void displayHSV(float h, float s, float v, int red , int green , int blue,int fr , int fg , int fb){



        activity_rgb_hsv_tvHH=findViewById(R.id.activity_rgb_hsv_tvHH);
        activity_rgb_hsv_tvSS=findViewById(R.id.activity_rgb_hsv_tvSS);
        activity_rgb_hsv_tvVV=findViewById(R.id.activity_rgb_hsv_tvVV);
        activity_rgb_hsv_tvH=findViewById(R.id.activity_rgb_hsv_tvH);
        activity_rgb_hsv_tvS=findViewById(R.id.activity_rgb_hsv_tvS);
        activity_rgb_hsv_tvV=findViewById(R.id.activity_rgb_hsv_tvV);
        activity_rgb_hsv_tvmid1=findViewById(R.id.activity_rgb_hsv_tvmid1);
        activity_rgb_hsv_tvmid2=findViewById(R.id.activity_rgb_hsv_tvmid2);
        activity_rgb_hsv_tvmid3=findViewById(R.id.activity_rgb_hsv_tvmid3);

        activity_rgb_hsv_layout=findViewById(R.id.activity_rgb_hsv_layout);
        TextView activity_rgb_hsv_tvHH = (TextView) findViewById(R.id.activity_rgb_hsv_tvHH);
        TextView activity_rgb_hsv_tvSS = (TextView) findViewById(R.id.activity_rgb_hsv_tvSS);
        TextView activity_rgb_hsv_tvVV = (TextView) findViewById(R.id.activity_rgb_hsv_tvVV);
        TextView activity_rgb_hsv_tvH=(TextView) findViewById(R.id.activity_rgb_hsv_tvH);
        TextView activity_rgb_hsv_tvS=(TextView) findViewById(R.id.activity_rgb_hsv_tvS);
        TextView activity_rgb_hsv_tvV=(TextView) findViewById(R.id.activity_rgb_hsv_tvV);
        TextView activity_rgb_hsv_tvmid1=(TextView) findViewById(R.id.activity_rgb_hsv_tvmid1);
        TextView activity_rgb_hsv_tvmid2=(TextView) findViewById(R.id.activity_rgb_hsv_tvmid2);
        TextView activity_rgb_hsv_tvmid3=(TextView) findViewById(R.id.activity_rgb_hsv_tvmid3);
        activity_rgb_hsv_layout.setBackgroundColor(Color.rgb(red, green, blue));
        activity_rgb_hsv_tvHH.setText(""+h);
        //activity_rgb_hsv_tvSS.setText(""+s);
        //activity_rgb_hsv_tvVV.setText(""+v);
        activity_rgb_hsv_tvSS.setText(new DecimalFormat("##.##").format(s));
        activity_rgb_hsv_tvVV.setText(new DecimalFormat("##.##").format(v));

        activity_rgb_hsv_tvH.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvS.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvV.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvmid1.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvmid2.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvmid3.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvHH.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvSS.setTextColor(Color.rgb(fr,fg,fb));
        activity_rgb_hsv_tvVV.setTextColor(Color.rgb(fr,fg,fb));




    }
    public void init() {
        activity_rgb_hsv_etR = findViewById(R.id.activity_rgb_hsv_etR);
        activity_rgb_hsv_etG = findViewById(R.id.activity_rgb_hsv_etG);
        activity_rgb_hsv_etB = findViewById(R.id.activity_rgb_hsv_etB);
       // activity_rgb_hsv_btn=findViewById(R.id.activity_rgb_hsv_btn);




    }

    private void process() {


    }

    private void listeners() {
        /* activity_rgb_hsv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromField();

            }
        });*/
        activity_rgb_hsv_etR.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0){

                    getDataFromField();

                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        activity_rgb_hsv_etG.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    Log.d("S.activity_rgb_hsv_etG", "enter in 0=" + s.length());
                    getDataFromField();

                }
                else{
                    Log.d("S.activity_rgb_hsv_etG", "enter in 0=" + s.length());
                    getDataFromField();

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        activity_rgb_hsv_etB.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();

                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }






    public void getDataFromField(){
        //just get data ans set into temp variable
        if(activity_rgb_hsv_etR.getText().toString().isEmpty() && activity_rgb_hsv_etG.getText().toString().isEmpty() && activity_rgb_hsv_etB.getText().toString().isEmpty()){
            convertRGBtoHSV(0,0,0);
        }
        else if(activity_rgb_hsv_etG.getText().toString().isEmpty() && activity_rgb_hsv_etB.getText().toString().isEmpty()){
            r = Integer.parseInt(activity_rgb_hsv_etR.getText().toString());
            if(r>255){
                activity_rgb_hsv_etR.setText(String.valueOf(255));
                r=255;
            }
            convertRGBtoHSV(r,0,0);

        }
        else if(activity_rgb_hsv_etR.getText().toString().isEmpty() && activity_rgb_hsv_etB.getText().toString().isEmpty()){
            g = Integer.parseInt(activity_rgb_hsv_etG.getText().toString());
            if(g>255){
                activity_rgb_hsv_etG.setText(String.valueOf(255));
                g=255;
            }
            convertRGBtoHSV(0,g,0);

        }
        else if(activity_rgb_hsv_etR.getText().toString().isEmpty() && activity_rgb_hsv_etG.getText().toString().isEmpty()){
            b = Integer.parseInt(activity_rgb_hsv_etB.getText().toString());
            if(b>255){
                activity_rgb_hsv_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoHSV(0,0,b);

        }
        else if(activity_rgb_hsv_etR.getText().toString().isEmpty()){
            g = Integer.parseInt(activity_rgb_hsv_etG.getText().toString());
            if(g>255){
                activity_rgb_hsv_etG.setText(String.valueOf(255));
                g=255;
            }
            b = Integer.parseInt(activity_rgb_hsv_etB.getText().toString());
            if(b>255){
                activity_rgb_hsv_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoHSV(0,g,b);

        }
        else if(activity_rgb_hsv_etG.getText().toString().isEmpty()){
            r = Integer.parseInt(activity_rgb_hsv_etR.getText().toString());
            if(r>255){
                activity_rgb_hsv_etR.setText(String.valueOf(255));
                r=255;
            }
            b = Integer.parseInt(activity_rgb_hsv_etB.getText().toString());
            if(b>255){
                activity_rgb_hsv_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoHSV(r,0,b);

        }
        else if(activity_rgb_hsv_etB.getText().toString().isEmpty()){
            r = Integer.parseInt(activity_rgb_hsv_etR.getText().toString());
            if(r>255){
                activity_rgb_hsv_etR.setText(String.valueOf(255));
                r=255;
            }
            g = Integer.parseInt(activity_rgb_hsv_etG.getText().toString());
            if(g>255){
                activity_rgb_hsv_etG.setText(String.valueOf(255));
                g=255;
            }
            convertRGBtoHSV(r,g,0);

        }
        else{
            r = Integer.parseInt(activity_rgb_hsv_etR.getText().toString());
            if(r>255){
                activity_rgb_hsv_etR.setText(String.valueOf(255));
                r=255;
            }
            g = Integer.parseInt(activity_rgb_hsv_etG.getText().toString());
            if(g>255){
                activity_rgb_hsv_etG.setText(String.valueOf(255));
                g=255;
            }
            b = Integer.parseInt(activity_rgb_hsv_etB.getText().toString());
            if(b>255){
                activity_rgb_hsv_etB.setText(String.valueOf(255));
                b=255;
            }
            convertRGBtoHSV(r,g,b);
        }

        Log.d("rgb_hsv_test", "R=" + r);
        Log.d("rgb_hsv_test", "G=" + g);
        Log.d("rgb_hsv_test", "B=" + b);



    }
}