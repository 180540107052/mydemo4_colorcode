package com.example.mydemo4_colorcode.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mydemo4_colorcode.R;

public class hsv_rgbActivity extends AppCompatActivity {

    EditText activity_hsv_rgb_etH, activity_hsv_rgb_etS, activity_hsv_rgb_etV;
    Button activity_hsv_rgb_btn;
    TextView activity_hsv_rgb_tvRR, activity_hsv_rgb_tvGG, activity_hsv_rgb_tvBB,
            activity_hsv_rgb_tvR, activity_hsv_rgb_tvG, activity_hsv_rgb_tvB,
            activity_hsv_rgb_tvmid1, activity_hsv_rgb_tvmid2, activity_hsv_rgb_tvmid3;
    LinearLayout activity_hsv_rgb_layout;

    float h;
    float s;
    float v;
    float _r, _g, _b;
    int r, g, b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hsv_rgb);

        init();
        process();
        listeners();
    }


    public void convertHSVtoRGB(float H, float S, float V) {
        Log.d("get", "_h=" + H);
        Log.d("get", "_s=" + S);
        Log.d("get", "_v=" + V);

        float s = S / 100;
        float v = V / 100;
        float c = s * v;
        float x = c * (1 - Math.abs((H / 60) % 2 - 1));
        float m = v - c;
        if (H >= 0 && H < 60) {
            _r = c;
            _g = x;
            _b = 0;
        } else if (H >= 60 && H < 120) {
            _r = x;
            _g = c;
            _b = 0;
        } else if (H >= 120 && H < 180) {
            _r = 0;
            _g = c;
            _b = x;
        } else if (H >= 180 && H < 240) {
            _r = 0;
            _g = x;
            _b = c;
        } else if (H >= 240 && H < 300) {
            _r = x;
            _g = 0;
            _b = c;
        } else if (H >= 300 && H < 360) {
            _r = c;
            _g = 0;
            _b = x;
        } else {

        }

        Log.d("get", "_r=" + _r);
        Log.d("get", "_g=" + _g);
        Log.d("get", "_b=" + _b);
        Log.d("get", "_m=" + m);
        convertToRGB(_r, _g, _b, m);


    }

    public void convertToRGB(float _r, float _g, float _b, float m) {
        Log.d("test", "_r=" + _r);
        Log.d("test", "_g=" + _g);
        Log.d("test", "_b=" + _b);
        Log.d("TEST", "_m=" + m);
        r=Math.round((_r+m)*255);
        g=Math.round((_g+m)*255);
        b=Math.round((_b+m)*255);
        Log.d("test", "r=" + r);
        Log.d("test", "g=" + g);
        Log.d("test", "b=" + b);
        int fr = 100 - r;
        int fg = 100 - g;
        int fb = 100 - b;


        displayRGB(r, g, b, fr, fg, fb);
    }

    // activity_hsv_rgb_tvRR,activity_hsv_rgb_tvGG,activity_hsv_rgb_tvBB
    public void displayRGB(int r, int g, int b, int fr, int fg, int fb) {

        activity_hsv_rgb_layout = findViewById(R.id.activity_hsv_rgb_layout);
        activity_hsv_rgb_tvRR = findViewById(R.id.activity_hsv_rgb_tvRR);
        activity_hsv_rgb_tvGG = findViewById(R.id.activity_hsv_rgb_tvGG);
        activity_hsv_rgb_tvBB = findViewById(R.id.activity_hsv_rgb_tvBB);
        activity_hsv_rgb_tvR = findViewById(R.id.activity_hsv_rgb_tvR);
        activity_hsv_rgb_tvG = findViewById(R.id.activity_hsv_rgb_tvG);
        activity_hsv_rgb_tvB = findViewById(R.id.activity_hsv_rgb_tvB);
        activity_hsv_rgb_tvmid1 = findViewById(R.id.activity_hsv_rgb_tvmid1);
        activity_hsv_rgb_tvmid2 = findViewById(R.id.activity_hsv_rgb_tvmid2);
        activity_hsv_rgb_tvmid3 = findViewById(R.id.activity_hsv_rgb_tvmid3);


        TextView activity_hsv_rgb_tvRR = (TextView) findViewById(R.id.activity_hsv_rgb_tvRR);
        TextView activity_hsv_rgb_tvGG = (TextView) findViewById(R.id.activity_hsv_rgb_tvGG);
        TextView activity_hsv_rgb_tvBB = (TextView) findViewById(R.id.activity_hsv_rgb_tvBB);
        TextView activity_hsv_rgb_tvR = (TextView) findViewById(R.id.activity_hsv_rgb_tvR);
        TextView activity_hsv_rgb_tvG = (TextView) findViewById(R.id.activity_hsv_rgb_tvG);
        TextView activity_hsv_rgb_tvB = (TextView) findViewById(R.id.activity_hsv_rgb_tvB);
        TextView activity_hsv_rgb_tvmid1 = (TextView) findViewById(R.id.activity_hsv_rgb_tvmid1);
        TextView activity_hsv_rgb_tvmid2 = (TextView) findViewById(R.id.activity_hsv_rgb_tvmid2);
        TextView activity_hsv_rgb_tvmid3 = (TextView) findViewById(R.id.activity_hsv_rgb_tvmid3);
        activity_hsv_rgb_layout.setBackgroundColor(Color.rgb(r, g, b));
        activity_hsv_rgb_tvRR.setText("" + r);
        activity_hsv_rgb_tvGG.setText("" + g);
        activity_hsv_rgb_tvBB.setText("" + b);
        activity_hsv_rgb_tvRR.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvGG.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvBB.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvR.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvG.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvB.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvmid1.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvmid2.setTextColor(Color.rgb(fr, fg, fb));
        activity_hsv_rgb_tvmid3.setTextColor(Color.rgb(fr, fg, fb));


    }


    public void init() {
        activity_hsv_rgb_etH = findViewById(R.id.activity_hsv_rgb_etH);
        activity_hsv_rgb_etS = findViewById(R.id.activity_hsv_rgb_etS);
        activity_hsv_rgb_etV = findViewById(R.id.activity_hsv_rgb_etV);
        // activity_hsv_rgb_btn = findViewById(R.id.activity_hsv_rgb_btn);

    }

    private void process() {


    }
    private void listeners() {

        activity_hsv_rgb_etH.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activity_hsv_rgb_etS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        activity_hsv_rgb_etV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void getDataFromField(){
        if(activity_hsv_rgb_etH.getText().toString().isEmpty() && activity_hsv_rgb_etS.getText().toString().isEmpty() && activity_hsv_rgb_etV.getText().toString().isEmpty()){
            convertHSVtoRGB(0,0,0);
        }
        else if(activity_hsv_rgb_etS.getText().toString().isEmpty() && activity_hsv_rgb_etV.getText().toString().isEmpty()){
            h = Float.parseFloat(activity_hsv_rgb_etH.getText().toString());
            if(h>360){
                activity_hsv_rgb_etH.setText(String.valueOf(360));
                h=360;
            }
            convertHSVtoRGB(h,0,0);
        }
        else if(activity_hsv_rgb_etH.getText().toString().isEmpty() && activity_hsv_rgb_etV.getText().toString().isEmpty()){
            s = Float.parseFloat(activity_hsv_rgb_etS.getText().toString());
            if(s>100){
                activity_hsv_rgb_etS.setText(String.valueOf(100));
                s=100;
            }
            convertHSVtoRGB(0,s,0);
        }
        else if(activity_hsv_rgb_etH.getText().toString().isEmpty() && activity_hsv_rgb_etS.getText().toString().isEmpty()){
            v = Float.parseFloat(activity_hsv_rgb_etV.getText().toString());
            if(v>100){
                activity_hsv_rgb_etV.setText(String.valueOf(100));
                v=100;
            }
            convertHSVtoRGB(0,0,v);
        }
        else if(activity_hsv_rgb_etH.getText().toString().isEmpty()){
            s = Float.parseFloat(activity_hsv_rgb_etS.getText().toString());
            if(s>100){
                activity_hsv_rgb_etS.setText(String.valueOf(100));
                s=100;
            }
            v = Float.parseFloat(activity_hsv_rgb_etV.getText().toString());
            if(v>100){
                activity_hsv_rgb_etV.setText(String.valueOf(100));
                v=100;
            }
            convertHSVtoRGB(0,s,v);

        }
        else if(activity_hsv_rgb_etS.getText().toString().isEmpty()){
            h = Float.parseFloat(activity_hsv_rgb_etH.getText().toString());
            if(h>360){
                activity_hsv_rgb_etH.setText(String.valueOf(360));
                h=360;
            }
            v = Float.parseFloat(activity_hsv_rgb_etV.getText().toString());
            if(v>100){
                activity_hsv_rgb_etV.setText(String.valueOf(100));
                v=100;
            }
            convertHSVtoRGB(h,0,v);

        }
        else if(activity_hsv_rgb_etV.getText().toString().isEmpty()){
            h = Float.parseFloat(activity_hsv_rgb_etH.getText().toString());
            if(h>360){
                activity_hsv_rgb_etH.setText(String.valueOf(360));
                h=360;
            }
            s = Float.parseFloat(activity_hsv_rgb_etS.getText().toString());
            if(s>100){
                activity_hsv_rgb_etS.setText(String.valueOf(100));
                s=100;
            }
            convertHSVtoRGB(h,s,0);

        }
        else{
            h = Float.parseFloat(activity_hsv_rgb_etH.getText().toString());
            if(h>360){
                activity_hsv_rgb_etH.setText(String.valueOf(360));
                h=360;
            }
            s = Float.parseFloat(activity_hsv_rgb_etS.getText().toString());
            if(s>100){
                activity_hsv_rgb_etS.setText(String.valueOf(100));
                s=100;
            }
            v = Float.parseFloat(activity_hsv_rgb_etV.getText().toString());
            if(v>100){
                activity_hsv_rgb_etV.setText(String.valueOf(100));
                v=100;
            }
            convertHSVtoRGB(h,s,v);
        }

    }


}