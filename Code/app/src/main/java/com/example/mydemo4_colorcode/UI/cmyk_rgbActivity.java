package com.example.mydemo4_colorcode.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mydemo4_colorcode.R;

public class cmyk_rgbActivity extends AppCompatActivity {

    TextView activity_cmyk_rgb_etC,activity_cmyk_rgb_etM,activity_cmyk_rgb_etY,activity_cmyk_rgb_etK;
    Button activity_cmyk_rgb_btn;
    TextView activity_cmyk_rgb_tvRR,activity_cmyk_rgb_tvGG,activity_cmyk_rgb_tvBB,
            activity_cmyk_rgb_tvR,activity_cmyk_rgb_tvG,activity_cmyk_rgb_tvB,
            activity_cmyk_rgb_tvmid1,activity_cmyk_rgb_tvmid2,activity_cmyk_rgb_tvmid3;
    LinearLayout activity_cmyk_rgb_layout;

    float c;
    float m;
    float y;
    float k;

    float r,g,b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cmyk_rgb);

        init();
        process();
        listeners();
    }

    public void convertCMYKtoRGB(float c, float m, float y, float k){
        Log.d("test", "C=" + c);
        Log.d("test", "M=" + m);
        Log.d("test", "Y=" + y);
        Log.d("test", "K=" + k);
        c=c/100;
        m=m/100;
        y=y/100;
        k=k/100;

        r = (255 * (1-c) * (1-k));
        int valueR = Math.round(r);
        Log.d("test", "r=" + valueR);
        g = (255 * (1-m) * (1-k));
        int valueG = Math.round(g);
        Log.d("test", "g=" + valueG);
        b = (255 * (1-y) * (1-k));
        int valueB = Math.round(b);
        Log.d("test", "b=" + valueB);
        int fr=255-valueR;
        int fg=255-valueG;
        int fb=255-valueB;

        displayRGB(valueR,valueG,valueB,fr,fg,fb);


    }
    // activity_cmyk_rgb_tvRR,activity_cmyk_rgb_tvGG,activity_cmyk_rgb_tvBB;
    public void  displayRGB(int valueR,int valueG,int valueB,int fr,int fg,int fb){
        activity_cmyk_rgb_tvRR = findViewById(R.id.activity_cmyk_rgb_tvRR);
        activity_cmyk_rgb_tvGG = findViewById(R.id.activity_cmyk_rgb_tvGG);
        activity_cmyk_rgb_tvBB = findViewById(R.id.activity_cmyk_rgb_tvBB);
        activity_cmyk_rgb_tvR = findViewById(R.id.activity_cmyk_rgb_tvR);
        activity_cmyk_rgb_tvG = findViewById(R.id.activity_cmyk_rgb_tvG);
        activity_cmyk_rgb_tvB = findViewById(R.id.activity_cmyk_rgb_tvB);
        activity_cmyk_rgb_tvmid1 = findViewById(R.id.activity_cmyk_rgb_tvmid1);
        activity_cmyk_rgb_tvmid2 = findViewById(R.id.activity_cmyk_rgb_tvmid2);
        activity_cmyk_rgb_tvmid3 = findViewById(R.id.activity_cmyk_rgb_tvmid3);
        activity_cmyk_rgb_layout = findViewById(R.id.activity_cmyk_rgb_layout);
        TextView activity_cmyk_rgb_tvRR = (TextView) findViewById(R.id.activity_cmyk_rgb_tvRR);
        TextView activity_cmyk_rgb_tvGG = (TextView) findViewById(R.id.activity_cmyk_rgb_tvGG);
        TextView activity_cmyk_rgb_tvBB = (TextView) findViewById(R.id.activity_cmyk_rgb_tvBB);
        TextView activity_cmyk_rgb_tvR = (TextView) findViewById(R.id.activity_cmyk_rgb_tvR);
        TextView activity_cmyk_rgb_tvG = (TextView) findViewById(R.id.activity_cmyk_rgb_tvG);
        TextView activity_cmyk_rgb_tvB = (TextView) findViewById(R.id.activity_cmyk_rgb_tvB);
        TextView activity_cmyk_rgb_tvmid1 = (TextView) findViewById(R.id.activity_cmyk_rgb_tvmid1);
        TextView activity_cmyk_rgb_tvmid2 = (TextView) findViewById(R.id.activity_cmyk_rgb_tvmid2);
        TextView activity_cmyk_rgb_tvmid3 = (TextView) findViewById(R.id.activity_cmyk_rgb_tvmid3);
        activity_cmyk_rgb_layout.setBackgroundColor(Color.rgb(valueR, valueG, valueB));
        activity_cmyk_rgb_tvRR.setText(""+valueR);
        activity_cmyk_rgb_tvGG.setText(""+valueG);
        activity_cmyk_rgb_tvBB.setText(""+valueB);

        activity_cmyk_rgb_tvRR.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvGG.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvBB.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvR.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvG.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvB.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvmid1.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvmid2.setTextColor(Color.rgb(fr,fg,fb));
        activity_cmyk_rgb_tvmid3.setTextColor(Color.rgb(fr,fg,fb));


    }
    public void init() {
        activity_cmyk_rgb_etC = findViewById(R.id.activity_cmyk_rgb_etC);
        activity_cmyk_rgb_etM = findViewById(R.id.activity_cmyk_rgb_etM);
        activity_cmyk_rgb_etY = findViewById(R.id.activity_cmyk_rgb_etY);
        activity_cmyk_rgb_etK = findViewById(R.id.activity_cmyk_rgb_etK);

        // activity_cmyk_rgb_btn=findViewById(R.id.activity_cmyk_rgb_btn);

    }

    private void process() {


    }

    private void listeners() {
        activity_cmyk_rgb_etC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activity_cmyk_rgb_etM.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activity_cmyk_rgb_etY.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        activity_cmyk_rgb_etK.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    getDataFromField();
                }
                else{
                    getDataFromField();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /* public void getDataFromField_(){
        //just get data ans set into temp variable

        //For C
        if(TextUtils.isEmpty(activity_cmyk_rgb_etC.getText().toString())){
            activity_cmyk_rgb_etC.setText(String.valueOf(100));
        }
        c = Integer.parseInt( activity_cmyk_rgb_etC.getText().toString());
        if(c>100){
            activity_cmyk_rgb_etC.setText(String.valueOf(100));
            c=100;
        }

        //For M
        if(TextUtils.isEmpty(activity_cmyk_rgb_etM.getText().toString())){
            activity_cmyk_rgb_etM.setText(String.valueOf(100));
        }
        m = Integer.parseInt( activity_cmyk_rgb_etM.getText().toString());
        if(m>100){
            activity_cmyk_rgb_etM.setText(String.valueOf(100));
            m=100;
        }

        //For Y
        if(TextUtils.isEmpty(activity_cmyk_rgb_etY.getText().toString())){
            activity_cmyk_rgb_etY.setText(String.valueOf(100));
        }
        y = Integer.parseInt( activity_cmyk_rgb_etY.getText().toString());
        if(y>100){
            activity_cmyk_rgb_etY.setText(String.valueOf(100));
            y=100;
        }

        //For K
        if(TextUtils.isEmpty(activity_cmyk_rgb_etK.getText().toString())){
            activity_cmyk_rgb_etK.setText(String.valueOf(100));
        }
        k = Integer.parseInt( activity_cmyk_rgb_etK.getText().toString());
        if(k>100){
            activity_cmyk_rgb_etK.setText(String.valueOf(100));
            k=100;
        }
        Log.d("cmyk_rgb_test", "C=" + c);
        Log.d("cmyk_rgb_test", "M=" + m);
        Log.d("cmyk_rgb_test", "Y=" + y);
        Log.d("cmyk_rgb_test", "K=" + k);
        convertCMYKtoRGB(c,m,y,k);


    } */
    public void getDataFromField(){
        if(activity_cmyk_rgb_etC.getText().toString().isEmpty() && activity_cmyk_rgb_etM.getText().toString().isEmpty() && activity_cmyk_rgb_etY.getText().toString().isEmpty() &&  activity_cmyk_rgb_etK.getText().toString().isEmpty()){
            convertCMYKtoRGB(0,0,0,0);
        }
        else if(activity_cmyk_rgb_etM.getText().toString().isEmpty() && activity_cmyk_rgb_etY.getText().toString().isEmpty() && activity_cmyk_rgb_etK.getText().toString().isEmpty() ){
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            convertCMYKtoRGB(c,0,0,0);

        }
        else if(activity_cmyk_rgb_etC.getText().toString().isEmpty() && activity_cmyk_rgb_etY.getText().toString().isEmpty() && activity_cmyk_rgb_etK.getText().toString().isEmpty() ){
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            convertCMYKtoRGB(0,m,0,0);

        }
        else if(activity_cmyk_rgb_etC.getText().toString().isEmpty() && activity_cmyk_rgb_etM.getText().toString().isEmpty() && activity_cmyk_rgb_etK.getText().toString().isEmpty() ){
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            convertCMYKtoRGB(0,0,y,0);

        }
        else if(activity_cmyk_rgb_etC.getText().toString().isEmpty() && activity_cmyk_rgb_etM.getText().toString().isEmpty() && activity_cmyk_rgb_etY.getText().toString().isEmpty() ){
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(0,0,0,k);

        }
        else if(activity_cmyk_rgb_etY.getText().toString().isEmpty() && activity_cmyk_rgb_etK.getText().toString().isEmpty() ){
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            convertCMYKtoRGB(c,m,0,0);

        }
        else if(activity_cmyk_rgb_etC.getText().toString().isEmpty() && activity_cmyk_rgb_etK.getText().toString().isEmpty() ){
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            convertCMYKtoRGB(0,m,y,0);

        }
        else if(activity_cmyk_rgb_etC.getText().toString().isEmpty() && activity_cmyk_rgb_etM.getText().toString().isEmpty() ){
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(0,0,y,k);

        }
        else if(activity_cmyk_rgb_etM.getText().toString().isEmpty() && activity_cmyk_rgb_etY.getText().toString().isEmpty() ){
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(c,0,0,k);

        }
        else if(activity_cmyk_rgb_etM.getText().toString().isEmpty() && activity_cmyk_rgb_etK.getText().toString().isEmpty() ){
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                y=100;
            }
            convertCMYKtoRGB(c,0,y,0);

        }
        else if(activity_cmyk_rgb_etC.getText().toString().isEmpty()){
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(0,m,y,k);

        }
        else if(activity_cmyk_rgb_etM.getText().toString().isEmpty()) {
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(c,0,y,k);
        }
        else if(activity_cmyk_rgb_etY.getText().toString().isEmpty()) {
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(c,m,0,k);
        }
        else if(activity_cmyk_rgb_etK.getText().toString().isEmpty()){
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            convertCMYKtoRGB(c,m,y,0);
        }
        else{
            c = Integer.parseInt(activity_cmyk_rgb_etC.getText().toString());
            if(c>100){
                activity_cmyk_rgb_etC.setText(String.valueOf(100));
                c=100;
            }
            m = Integer.parseInt(activity_cmyk_rgb_etM.getText().toString());
            if(m>100){
                activity_cmyk_rgb_etM.setText(String.valueOf(100));
                m=100;
            }
            y = Integer.parseInt(activity_cmyk_rgb_etY.getText().toString());
            if(y>100){
                activity_cmyk_rgb_etY.setText(String.valueOf(100));
                y=100;
            }
            k = Integer.parseInt(activity_cmyk_rgb_etK.getText().toString());
            if(k>100){
                activity_cmyk_rgb_etK.setText(String.valueOf(100));
                k=100;
            }
            convertCMYKtoRGB(c,m,y,k);

        }



        
    }
}
